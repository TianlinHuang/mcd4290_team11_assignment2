//trip class, the information for a single trip
class Trip{
    constructor(Id,time,distance,taxi,fare,addresses,creationDate){
        this._tripId=Id
        this._startTimeAndDate=time
        this._totalDistance=distance
        this._taxiType=taxi
        this._fare=fare
        this._address=addresses
        this._creationDate=creationDate
        this._stop=addresses.length-2
    }
    
    get tripId(){
        return this._tripId
    }
    get startTimeAndDate(){
        return this._startTimeAndDate
    }
    get totalDistance(){
        return this._totalDistance
    }
    get taxiType(){
        return this._taxiType
    }
    get fare(){
        return this._fare
    }
    get address(){
        return this._address
    }
    get creationDate(){
        return this._creationDate
    }
    get stop(){
        return this._stop
    }
    
    set tripId(Id){
        this._tripId=Id
    }
    set startTimeAndDate(date){
        this._startTimeAndDate=date
    }
    set totalDistance(distance){
         this._totalDistance=distance
    }
    set taxiType(){
         this._taxiType
    }
    set fare(){
         this._fare
    }
    set address(addressArray){
         this._address=addressArray
    }
    addAdditionStop(additionStop){
        this._address.push(additionStop)
    }
    
    set creationDate(creationTime){
         this._creationDate=creationTime
    }
    set stop(numStop){
        this._stop=numStop
    }
    
    fromData(data){
        this._tripId=data._tripId
        this._startTimeAndDate=data._startTimeAndDate
        this._totalDistance=data._totalDistance
        this._taxiType=data._taxiType
        this._fare=data._fare
        this._address=data._address
        this._creationDate=data._creationDate
        this._stop=data._stop
    }
}

//triplist class, class for all of the trip
class TripList{
    constructor(){
        this._trips=[]
    }
    get trips(){
        return this._trips
    }
    set trips(tripsArray){
        this._trips=tripsArray
    }
    addTrip(Id,time,distance,taxi,fare,addresses,creationDate){
        this._trips.push(new Trip(Id,time,distance,taxi,fare,addresses,creationDate))
    }
    removeTrip(id){
        this._trips.splice(id,1)
    }
    fromData(data){
        this._trips=data._trips
    }
}


